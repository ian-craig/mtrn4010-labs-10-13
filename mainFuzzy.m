% MTRN4010 Week 11 Lab
% Ian Craig ian-craig@outlook.com
% Updated 21st June 2014
%
% This lab involves designing a fuzzy controller to drive a car to a goal position.
% Uses fuzzyController.fis
% To examine this controller run the command >> fuzzy fuzzyController.fis
% Uses Car.m which is a class to simplify the state, process model and plot of the car.

function mainFuzzy()
    % --------- START CONFIG ----------
    
    DT = 0.1; % Time step
    T = 100; % Max time
    GOAL = [-20, -45]; % Goal position
    SIM_SPEED = 5; % Run simulation faster than real time
    
    % --------- END CONFIG ----------
    
    % Create a car
    car = Car();
    plot(GOAL(1), GOAL(2), 'ro');
    
    % Load fuzzy controller
    fuzzyController = readfis('fuzzyController.fis');
    
    % Simulation Loop
    for t = 0:DT:T
        % Get measurement input
        input = getObservation([car.x,car.y,car.theta], GOAL);
        input(1) = min(input(1), 30); % Cap range measurement at 30 ("far away")
        
        % Run Fuzzy Controller on measurement input
        out = evalfis(input, fuzzyController);
        
        % Update Car Position using control output
        car.update(t, out(1), out(2));
        
        % Plot
        car.plot(true);
        pause(DT/SIM_SPEED);
        
        % End if we've reached the goal
        if input(1) < 0.3
            break;
        end
    end
end

% Get a [range, bearing] observation from observer [x, y, bearing] and landmark [x, y]
function output = getObservation(observer, landmark)
    dx = landmark(1) - observer(1);
    dy = landmark(2) - observer(2);
    output(1) = sqrt(dx.*dx + dy.*dy);
    output(2) = mod((atan2(dy,dx) - observer(3)) + pi, 2*pi) - pi;
end