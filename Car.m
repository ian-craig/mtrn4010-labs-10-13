% MTRN4010 Week 11 Lab - Car Class
% Ian Craig ian-craig@outlook.com
% Updated 20th June 2014

classdef Car < handle
    %Car class to maintain the car's state, plot and process model for Fuzzy Lab
    
    properties
        x = 0;
        y = 0
        theta = pi/2; % degrees
        updateTime = 0;
        path = [];
        figH;
        axesH;
        shapeH;
        pathH;
    end
    
    methods
        % Constructor - create a new car with initial position spoecified, default zero.
        function this = Car(x, y, theta)
            if nargin == 3
                this.x = x;
                this.y = y;
                this.theta = theta;
            end
            this.path = [this.x this.y];
            % Set up figure
            close all;
            this.figH = figure(1);
            this.axesH = gca;
            hold on;
            % Configure Graph 
            axis(this.axesH, [-60 60 -50 50]);
            grid on;
            title(sprintf('Time: %.1f seconds', this.updateTime));
            xlabel('x-direction'); ylabel('y-direction');
            set(this.axesH,'XTick',-50:10:50);
        end
        
        % Process Model update step
        function update(this, time, velocity, omega)
            dt = time-this.updateTime;
            this.updateTime = time;
            if velocity == 0, return; end;
            this.x = this.x + dt*velocity*cos(this.theta);
            this.y = this.y + dt*velocity*sin(this.theta);
            this.theta = mod((this.theta + dt*omega) + pi, 2*pi) - pi;
            this.path(end+1,:) = [this.x this.y];
        end
        
        % Plot the car position
        % If showPath = true then a position history trail will also be plotted.
        function plot(this, showPath)
            if nargin < 2
                showPath = false;
            end
            % Remove old plot
            if (ishandle(this.shapeH))
                delete(this.shapeH);
            end
            % Transform our car shape
            outline = [-1, -1; -1, 1; 1, 1; 1.5, 0; 1, -1; -1, -1];
            shape = Car.transformPoints(outline, this.x, this.y, this.theta);
            % Plot car
            hold on;
            this.shapeH = plot(this.axesH, shape(:,1), shape(:,2), 'LineWidth', 1.5);
            if showPath
                this.pathH = plot(this.axesH, this.path(:,1), this.path(:,2), 'g');
            end
            title(sprintf('Time: %.1f seconds', this.updateTime));
            hold off;
        end
    end
    
    methods(Static)
        % Transform a set of points by roatation (theta) and then translation (x,y).
        % @see car.plot()
        function points = transformPoints(points,x,y,theta)
            R = [cos(theta) -sin(theta); sin(theta) cos(theta)];
            so = (R*points')'; % apply the rotation about the origin
            points = [so(:,1)+x, so(:,2)+y]; % shift so the origin goes to the desired position
        end 
    end
    
end


