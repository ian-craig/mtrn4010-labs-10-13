% MTRN4010 Week 13 Lab
% Ian Craig ian-craig@outlook.com
% Updated 20th June 2014
%
% Each shape (e.g. N_CARS = 3 will produce triangles) represents a particle (i.e. a proposed formation) where the 
% vertices are positions in the formation. This problem involves finding the optimal formation for the initial points
% (red stars) to move to at lowest cost, it does not define how they move. The moving shapes are particle searching for
% an optimal position to recommend.

function mainParticle()
    global RADIUS;
    global N_CARS;
    global p;
    
    % --------- START CONFIG ----------
    
    N_PARTICLES = 10;
    N_ITERATIONS = 200;
    DRAW_DELAY = 0; % Increase this slightly to slow down simulation
    
    % Formation
    RADIUS = 5;
    RANGE = 50;
    N_CARS = 4;

    % Continuous PSO Parameters
    wv = 0.4;
    wi = 0.1;
    wg = 0.1;
    
    % Discrete PSO Parameters
    wv_d = 0.7;
    wi_d = 0.7;
    wg_d = 0.7;
    
    % --------- END CONFIG ----------
    
    % Initialize some vectors to make clamping range easier later
    rangeMaxV = ones(N_PARTICLES,1)*RANGE;
    rangeMinV = zeros(N_PARTICLES,1);
    
    % Start the plot
    figure(1);
    axis([0 RANGE 0 RANGE]);

    % Generate initial car positions
    p = rand(N_CARS,2)*RANGE;
    
    % Generate initial particles for Continuous PSO (position and velocitiy)
    x_c = rand(N_PARTICLES,3); % start with a random matrix
    for i=1:size(x_c,1) % now scale these random vectors to position and rotation
        x_c(i,:) = x_c(i,:) .* [RANGE RANGE 2*pi] - [0 0 pi];
    end
    v_c = rand(N_PARTICLES,3);
    
    % Generate initial particles for Discrete PSO (position and velocitiy)
    x_d = zeros(N_PARTICLES,N_CARS);
    for i=1:N_PARTICLES
        x_d(i,:) = randperm(N_CARS);
    end
    v_d = cell(N_PARTICLES,1); % Velocity of each particle is a 2xN vector of N swaps
    for i=1:N_PARTICLES
        v_d{i} = rand_swap(N_CARS);
    end

    % Create variables to track best
        % Start fitness as a number larger than any will be so the first fitness calculated will become the best
    besti_fitness = ones(N_PARTICLES,1)*RANGE*N_CARS + 1; 
    besti_c = zeros(N_PARTICLES,3);
    besti_d = zeros(N_PARTICLES,N_CARS);

    % Simulation loop
    for iter = 1:N_ITERATIONS
        % Find best particles
        for i=1:N_PARTICLES
            f = fitness(x_c(i,:),x_d(i,:));
            if f < besti_fitness(i)
                besti_c(i,:) = x_c(i,:);
                besti_d(i,:) = x_d(i,:);
                besti_fitness(i) = f;
            end
        end

        % Create bestg as a matrix where every row is the same
        % Each row is the best row from besti
        bestg_ind = find(besti_fitness == min(besti_fitness), 1, 'first');
        bestg_c = repmat(besti_c(bestg_ind,:),[N_PARTICLES 1]);
        bestg_d = besti_d(bestg_ind,:);

        % Generate new random beta values
        beta1 = rand(N_PARTICLES,3);
        beta2 = rand(N_PARTICLES,3);

        % Continuous PSO Update Step
            % We've set this up so that we can do all particles as a vector calculation in one step :)
        v_c = wv*v_c + wi*beta1.*(besti_c - x_c) + wg*beta2.*(bestg_c - x_c);
        x_c = x_c + v_c;
        
        % Limit continuous position to be in range
        x_c(:,1) = min([x_c(:,1) rangeMaxV],[],2);
        x_c(:,1) = max([x_c(:,1) rangeMinV],[],2);
        x_c(:,2) = min([x_c(:,2) rangeMaxV],[],2);
        x_c(:,2) = max([x_c(:,2) rangeMinV],[],2);
        x_c(:,3) = mod(x_c(:,3)+pi,2*pi)-pi;
        
        % Discrete PSO Update Step
        for i=1:N_PARTICLES
            v_d{i} = disc_cat(disc_cat(disc_trunc(wv_d,v_d{i}), disc_trunc(wi_d*beta1(i),disc_minus(besti_d(i,:), x_d(i,:)))), disc_trunc(wg_d*beta2(i),disc_minus(bestg_d, x_d(i,:))));
            x_d(i,:) = disc_plus(x_d(i,:), v_d{i});
        end

        
        
        % Plot
        cla;
        hold on;
        title(sprintf('Iteration %d - Best Fitness %.2f', iter, besti_fitness(bestg_ind)));
        plot(p(:,1),p(:,2), 'r*');
        text(p(:,1),p(:,2), cellstr( num2str([1:N_CARS]')), 'VerticalAlignment','bottom', 'HorizontalAlignment','right');
        for i=1:N_PARTICLES
            pos = particleCarPositions(x_c(i,:));
            plot([pos(:,1);pos(1,1)],[pos(:,2);pos(1,2)], 'b');
        end
        bestpos = particleCarPositions(bestg_c(1,:));
        plot([bestpos(:,1);bestpos(1,1)],[bestpos(:,2);bestpos(1,2)], 'g');
        for i=1:N_CARS
            plot([bestpos(bestg_d(i),1);p(i,1)],[bestpos(bestg_d(i),2);p(i,2)], 'g');
        end
        hold off;
        drawnow;
        pause(DRAW_DELAY);
    end
end

% Get coordinates for the positions within a formation x
function positions = particleCarPositions(x)
    global N_CARS;
    global RADIUS;
    angles = linspace(0,2*pi,N_CARS+1)' + x(3);
    angles = angles(1:end-1);
    positions = [cos(angles)*RADIUS + x(1), sin(angles)*RADIUS + x(2)];
end

% Fitness function for a particle x
    % if d (discrete ordering) isn't specified, default to as provided by particleCarPositions()
function d = fitness(x, d)
    global p;
    pos = particleCarPositions(x);
    if nargin == 1
        diff = (pos-p);
    else
        diff = (pos(d,:)-p);
    end
    dist = sqrt(diff(:,1).^2 + diff(:,2).^2);
    d = mean(dist) + max(dist);
end

% Perform a swap of two elements in vector pos specified by pair vel
function pos = swap(pos,vel)
    tmp = pos(vel(1));
    pos(vel(1)) = pos(vel(2));
    pos(vel(2)) = tmp;
end

% Generate a random swap in range n
function v = rand_swap(n)
    v = randi(n,1,2);
    while v(1) == v(2)
        v = randi(n,1,2);
    end
end

% ------- Discrete PSO Operators ---------

function pos = disc_plus(pos, vel)
    for i=1:size(vel,1)
        pos = swap(pos, vel(i,:));
    end
end

function vel = disc_minus(pos1, pos2)
    vel = [];
    for i=1:length(pos1)
        if (pos2(i) ~= pos1(i))
            j = find(pos2 == pos1(i));
            vel(end+1,:) = [i j];
            pos2 = swap(pos2, vel(end,:));
        end
    end
end

function vel = disc_cat(vel1, vel2)
    vel = [vel1;vel2];
end

function vel = disc_trunc(l, vel)
    len = ceil(min(l,1)*size(vel,1));
    vel = vel(1:len,:);
end
